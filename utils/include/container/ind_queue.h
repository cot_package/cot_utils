/**
  **********************************************************************************************************************
  * @file    queue.h
  * @brief   该文件提供不定长的队列功能实现【FIFO】所有函数原型
  * @author  const_zpc    any question please send mail to const_zpc@163.com
  * @version V0.3
  * @date    2024-10-13
  **********************************************************************************************************************
  *
  **********************************************************************************************************************
  */

/* Define to prevent recursive inclusion -----------------------------------------------------------------------------*/

#ifndef _COT_CONTAINER_IND_QUEUE_H_
#define _COT_CONTAINER_IND_QUEUE_H_

/* Includes ----------------------------------------------------------------------------------------------------------*/
#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>

#ifdef __cplusplus
 extern "C" {
#endif 


typedef struct
{
    uint8_t *pBuf;
    size_t  limit;
    size_t  waterLevel;
    size_t  wIdx;
    size_t  rIdx;
    size_t  count;
} cotIndQueue_t;



extern void cotIndQueue_Init(cotIndQueue_t *pIndQueue, uint8_t *pBuf, size_t bufSize);
extern void cotIndQueue_Clear(cotIndQueue_t *pIndQueue);

extern bool cotIndQueue_Empty(cotIndQueue_t *pIndQueue);
extern bool cotIndQueue_Full(cotIndQueue_t *pIndQueue, size_t length);
extern size_t cotIndQueue_Size(cotIndQueue_t *pIndQueue);

extern void *cotIndQueue_Front(cotIndQueue_t *pIndQueue, size_t *plength);
extern int cotIndQueue_Push(cotIndQueue_t *pIndQueue, const void *pdata, size_t length);
extern int cotIndQueue_Pop(cotIndQueue_t *pIndQueue);

extern void cotIndQueue_Swap(cotIndQueue_t *pQueue1, cotIndQueue_t *pQueue2);



#ifdef __cplusplus
 }
#endif

#endif // !_COT_CONTAINER_IND_QUEUE_H_
