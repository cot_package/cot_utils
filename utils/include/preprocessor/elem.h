# ifndef COT_PREPROCESSOR_TUPLE_ELEM_H
# define COT_PREPROCESSOR_TUPLE_ELEM_H


#include "preprocessor/overload.h"
#include "preprocessor/cat.h"
#include "preprocessor/rem.h"

# define COT_PP_TUPLE_ELEM(...) COT_PP_OVERLOAD(COT_PP_TUPLE_ELEM_O_, __VA_ARGS__)(__VA_ARGS__)
#    	 define COT_PP_TUPLE_ELEM_O_2(n, tuple) COT_PP_VARIADIC_ELEM(n, COT_PP_REM tuple)
#    define COT_PP_TUPLE_ELEM_O_3(size, n, tuple) COT_PP_TUPLE_ELEM_O_2(n, tuple)

# define COT_PP_TUPLE_ELEM_1_0(a) a
#
# define COT_PP_TUPLE_ELEM_2_0(a, b) a
# define COT_PP_TUPLE_ELEM_2_1(a, b) b
#
# define COT_PP_TUPLE_ELEM_3_0(a, b, c) a
# define COT_PP_TUPLE_ELEM_3_1(a, b, c) b
# define COT_PP_TUPLE_ELEM_3_2(a, b, c) c




#        define COT_PP_VARIADIC_ELEM(n, ...) COT_PP_CAT(COT_PP_VARIADIC_ELEM_, n)(__VA_ARGS__,)


#    define COT_PP_VARIADIC_ELEM_0(e0, ...) e0
#    define COT_PP_VARIADIC_ELEM_1(e0, e1, ...) e1
#    define COT_PP_VARIADIC_ELEM_2(e0, e1, e2, ...) e2
#    define COT_PP_VARIADIC_ELEM_3(e0, e1, e2, e3, ...) e3
#    define COT_PP_VARIADIC_ELEM_4(e0, e1, e2, e3, e4, ...) e4
#    define COT_PP_VARIADIC_ELEM_5(e0, e1, e2, e3, e4, e5, ...) e5
#    define COT_PP_VARIADIC_ELEM_6(e0, e1, e2, e3, e4, e5, e6, ...) e6
#    define COT_PP_VARIADIC_ELEM_7(e0, e1, e2, e3, e4, e5, e6, e7, ...) e7
#    define COT_PP_VARIADIC_ELEM_8(e0, e1, e2, e3, e4, e5, e6, e7, e8, ...) e8
#    define COT_PP_VARIADIC_ELEM_9(e0, e1, e2, e3, e4, e5, e6, e7, e8, e9, ...) e9
#    define COT_PP_VARIADIC_ELEM_10(e0, e1, e2, e3, e4, e5, e6, e7, e8, e9, e10, ...) e10
#    define COT_PP_VARIADIC_ELEM_11(e0, e1, e2, e3, e4, e5, e6, e7, e8, e9, e10, e11, ...) e11
#    define COT_PP_VARIADIC_ELEM_12(e0, e1, e2, e3, e4, e5, e6, e7, e8, e9, e10, e11, e12, ...) e12
#    define COT_PP_VARIADIC_ELEM_13(e0, e1, e2, e3, e4, e5, e6, e7, e8, e9, e10, e11, e12, e13, ...) e13
#    define COT_PP_VARIADIC_ELEM_14(e0, e1, e2, e3, e4, e5, e6, e7, e8, e9, e10, e11, e12, e13, e14, ...) e14
#    define COT_PP_VARIADIC_ELEM_15(e0, e1, e2, e3, e4, e5, e6, e7, e8, e9, e10, e11, e12, e13, e14, e15, ...) e15
#    define COT_PP_VARIADIC_ELEM_16(e0, e1, e2, e3, e4, e5, e6, e7, e8, e9, e10, e11, e12, e13, e14, e15, e16, ...) e16
#    define COT_PP_VARIADIC_ELEM_17(e0, e1, e2, e3, e4, e5, e6, e7, e8, e9, e10, e11, e12, e13, e14, e15, e16, e17, ...) e17
#    define COT_PP_VARIADIC_ELEM_18(e0, e1, e2, e3, e4, e5, e6, e7, e8, e9, e10, e11, e12, e13, e14, e15, e16, e17, e18, ...) e18
#    define COT_PP_VARIADIC_ELEM_19(e0, e1, e2, e3, e4, e5, e6, e7, e8, e9, e10, e11, e12, e13, e14, e15, e16, e17, e18, e19, ...) e19
#    define COT_PP_VARIADIC_ELEM_20(e0, e1, e2, e3, e4, e5, e6, e7, e8, e9, e10, e11, e12, e13, e14, e15, e16, e17, e18, e19, e20, ...) e20
#    define COT_PP_VARIADIC_ELEM_21(e0, e1, e2, e3, e4, e5, e6, e7, e8, e9, e10, e11, e12, e13, e14, e15, e16, e17, e18, e19, e20, e21, ...) e21
#    define COT_PP_VARIADIC_ELEM_22(e0, e1, e2, e3, e4, e5, e6, e7, e8, e9, e10, e11, e12, e13, e14, e15, e16, e17, e18, e19, e20, e21, e22, ...) e22
#    define COT_PP_VARIADIC_ELEM_23(e0, e1, e2, e3, e4, e5, e6, e7, e8, e9, e10, e11, e12, e13, e14, e15, e16, e17, e18, e19, e20, e21, e22, e23, ...) e23
#    define COT_PP_VARIADIC_ELEM_24(e0, e1, e2, e3, e4, e5, e6, e7, e8, e9, e10, e11, e12, e13, e14, e15, e16, e17, e18, e19, e20, e21, e22, e23, e24, ...) e24
#    define COT_PP_VARIADIC_ELEM_25(e0, e1, e2, e3, e4, e5, e6, e7, e8, e9, e10, e11, e12, e13, e14, e15, e16, e17, e18, e19, e20, e21, e22, e23, e24, e25, ...) e25
#    define COT_PP_VARIADIC_ELEM_26(e0, e1, e2, e3, e4, e5, e6, e7, e8, e9, e10, e11, e12, e13, e14, e15, e16, e17, e18, e19, e20, e21, e22, e23, e24, e25, e26, ...) e26
#    define COT_PP_VARIADIC_ELEM_27(e0, e1, e2, e3, e4, e5, e6, e7, e8, e9, e10, e11, e12, e13, e14, e15, e16, e17, e18, e19, e20, e21, e22, e23, e24, e25, e26, e27, ...) e27
#    define COT_PP_VARIADIC_ELEM_28(e0, e1, e2, e3, e4, e5, e6, e7, e8, e9, e10, e11, e12, e13, e14, e15, e16, e17, e18, e19, e20, e21, e22, e23, e24, e25, e26, e27, e28, ...) e28
#    define COT_PP_VARIADIC_ELEM_29(e0, e1, e2, e3, e4, e5, e6, e7, e8, e9, e10, e11, e12, e13, e14, e15, e16, e17, e18, e19, e20, e21, e22, e23, e24, e25, e26, e27, e28, e29, ...) e29
#    define COT_PP_VARIADIC_ELEM_30(e0, e1, e2, e3, e4, e5, e6, e7, e8, e9, e10, e11, e12, e13, e14, e15, e16, e17, e18, e19, e20, e21, e22, e23, e24, e25, e26, e27, e28, e29, e30, ...) e30
#    define COT_PP_VARIADIC_ELEM_31(e0, e1, e2, e3, e4, e5, e6, e7, e8, e9, e10, e11, e12, e13, e14, e15, e16, e17, e18, e19, e20, e21, e22, e23, e24, e25, e26, e27, e28, e29, e30, e31, ...) e31
#    define COT_PP_VARIADIC_ELEM_32(e0, e1, e2, e3, e4, e5, e6, e7, e8, e9, e10, e11, e12, e13, e14, e15, e16, e17, e18, e19, e20, e21, e22, e23, e24, e25, e26, e27, e28, e29, e30, e31, e32, ...) e32
#    define COT_PP_VARIADIC_ELEM_33(e0, e1, e2, e3, e4, e5, e6, e7, e8, e9, e10, e11, e12, e13, e14, e15, e16, e17, e18, e19, e20, e21, e22, e23, e24, e25, e26, e27, e28, e29, e30, e31, e32, e33, ...) e33
#    define COT_PP_VARIADIC_ELEM_34(e0, e1, e2, e3, e4, e5, e6, e7, e8, e9, e10, e11, e12, e13, e14, e15, e16, e17, e18, e19, e20, e21, e22, e23, e24, e25, e26, e27, e28, e29, e30, e31, e32, e33, e34, ...) e34
#    define COT_PP_VARIADIC_ELEM_35(e0, e1, e2, e3, e4, e5, e6, e7, e8, e9, e10, e11, e12, e13, e14, e15, e16, e17, e18, e19, e20, e21, e22, e23, e24, e25, e26, e27, e28, e29, e30, e31, e32, e33, e34, e35, ...) e35
#    define COT_PP_VARIADIC_ELEM_36(e0, e1, e2, e3, e4, e5, e6, e7, e8, e9, e10, e11, e12, e13, e14, e15, e16, e17, e18, e19, e20, e21, e22, e23, e24, e25, e26, e27, e28, e29, e30, e31, e32, e33, e34, e35, e36, ...) e36
#    define COT_PP_VARIADIC_ELEM_37(e0, e1, e2, e3, e4, e5, e6, e7, e8, e9, e10, e11, e12, e13, e14, e15, e16, e17, e18, e19, e20, e21, e22, e23, e24, e25, e26, e27, e28, e29, e30, e31, e32, e33, e34, e35, e36, e37, ...) e37
#    define COT_PP_VARIADIC_ELEM_38(e0, e1, e2, e3, e4, e5, e6, e7, e8, e9, e10, e11, e12, e13, e14, e15, e16, e17, e18, e19, e20, e21, e22, e23, e24, e25, e26, e27, e28, e29, e30, e31, e32, e33, e34, e35, e36, e37, e38, ...) e38
#    define COT_PP_VARIADIC_ELEM_39(e0, e1, e2, e3, e4, e5, e6, e7, e8, e9, e10, e11, e12, e13, e14, e15, e16, e17, e18, e19, e20, e21, e22, e23, e24, e25, e26, e27, e28, e29, e30, e31, e32, e33, e34, e35, e36, e37, e38, e39, ...) e39
#    define COT_PP_VARIADIC_ELEM_40(e0, e1, e2, e3, e4, e5, e6, e7, e8, e9, e10, e11, e12, e13, e14, e15, e16, e17, e18, e19, e20, e21, e22, e23, e24, e25, e26, e27, e28, e29, e30, e31, e32, e33, e34, e35, e36, e37, e38, e39, e40, ...) e40
#    define COT_PP_VARIADIC_ELEM_41(e0, e1, e2, e3, e4, e5, e6, e7, e8, e9, e10, e11, e12, e13, e14, e15, e16, e17, e18, e19, e20, e21, e22, e23, e24, e25, e26, e27, e28, e29, e30, e31, e32, e33, e34, e35, e36, e37, e38, e39, e40, e41, ...) e41
#    define COT_PP_VARIADIC_ELEM_42(e0, e1, e2, e3, e4, e5, e6, e7, e8, e9, e10, e11, e12, e13, e14, e15, e16, e17, e18, e19, e20, e21, e22, e23, e24, e25, e26, e27, e28, e29, e30, e31, e32, e33, e34, e35, e36, e37, e38, e39, e40, e41, e42, ...) e42
#    define COT_PP_VARIADIC_ELEM_43(e0, e1, e2, e3, e4, e5, e6, e7, e8, e9, e10, e11, e12, e13, e14, e15, e16, e17, e18, e19, e20, e21, e22, e23, e24, e25, e26, e27, e28, e29, e30, e31, e32, e33, e34, e35, e36, e37, e38, e39, e40, e41, e42, e43, ...) e43
#    define COT_PP_VARIADIC_ELEM_44(e0, e1, e2, e3, e4, e5, e6, e7, e8, e9, e10, e11, e12, e13, e14, e15, e16, e17, e18, e19, e20, e21, e22, e23, e24, e25, e26, e27, e28, e29, e30, e31, e32, e33, e34, e35, e36, e37, e38, e39, e40, e41, e42, e43, e44, ...) e44
#    define COT_PP_VARIADIC_ELEM_45(e0, e1, e2, e3, e4, e5, e6, e7, e8, e9, e10, e11, e12, e13, e14, e15, e16, e17, e18, e19, e20, e21, e22, e23, e24, e25, e26, e27, e28, e29, e30, e31, e32, e33, e34, e35, e36, e37, e38, e39, e40, e41, e42, e43, e44, e45, ...) e45
#    define COT_PP_VARIADIC_ELEM_46(e0, e1, e2, e3, e4, e5, e6, e7, e8, e9, e10, e11, e12, e13, e14, e15, e16, e17, e18, e19, e20, e21, e22, e23, e24, e25, e26, e27, e28, e29, e30, e31, e32, e33, e34, e35, e36, e37, e38, e39, e40, e41, e42, e43, e44, e45, e46, ...) e46
#    define COT_PP_VARIADIC_ELEM_47(e0, e1, e2, e3, e4, e5, e6, e7, e8, e9, e10, e11, e12, e13, e14, e15, e16, e17, e18, e19, e20, e21, e22, e23, e24, e25, e26, e27, e28, e29, e30, e31, e32, e33, e34, e35, e36, e37, e38, e39, e40, e41, e42, e43, e44, e45, e46, e47, ...) e47
#    define COT_PP_VARIADIC_ELEM_48(e0, e1, e2, e3, e4, e5, e6, e7, e8, e9, e10, e11, e12, e13, e14, e15, e16, e17, e18, e19, e20, e21, e22, e23, e24, e25, e26, e27, e28, e29, e30, e31, e32, e33, e34, e35, e36, e37, e38, e39, e40, e41, e42, e43, e44, e45, e46, e47, e48, ...) e48
#    define COT_PP_VARIADIC_ELEM_49(e0, e1, e2, e3, e4, e5, e6, e7, e8, e9, e10, e11, e12, e13, e14, e15, e16, e17, e18, e19, e20, e21, e22, e23, e24, e25, e26, e27, e28, e29, e30, e31, e32, e33, e34, e35, e36, e37, e38, e39, e40, e41, e42, e43, e44, e45, e46, e47, e48, e49, ...) e49
#    define COT_PP_VARIADIC_ELEM_50(e0, e1, e2, e3, e4, e5, e6, e7, e8, e9, e10, e11, e12, e13, e14, e15, e16, e17, e18, e19, e20, e21, e22, e23, e24, e25, e26, e27, e28, e29, e30, e31, e32, e33, e34, e35, e36, e37, e38, e39, e40, e41, e42, e43, e44, e45, e46, e47, e48, e49, e50, ...) e50
#    define COT_PP_VARIADIC_ELEM_51(e0, e1, e2, e3, e4, e5, e6, e7, e8, e9, e10, e11, e12, e13, e14, e15, e16, e17, e18, e19, e20, e21, e22, e23, e24, e25, e26, e27, e28, e29, e30, e31, e32, e33, e34, e35, e36, e37, e38, e39, e40, e41, e42, e43, e44, e45, e46, e47, e48, e49, e50, e51, ...) e51
#    define COT_PP_VARIADIC_ELEM_52(e0, e1, e2, e3, e4, e5, e6, e7, e8, e9, e10, e11, e12, e13, e14, e15, e16, e17, e18, e19, e20, e21, e22, e23, e24, e25, e26, e27, e28, e29, e30, e31, e32, e33, e34, e35, e36, e37, e38, e39, e40, e41, e42, e43, e44, e45, e46, e47, e48, e49, e50, e51, e52, ...) e52
#    define COT_PP_VARIADIC_ELEM_53(e0, e1, e2, e3, e4, e5, e6, e7, e8, e9, e10, e11, e12, e13, e14, e15, e16, e17, e18, e19, e20, e21, e22, e23, e24, e25, e26, e27, e28, e29, e30, e31, e32, e33, e34, e35, e36, e37, e38, e39, e40, e41, e42, e43, e44, e45, e46, e47, e48, e49, e50, e51, e52, e53, ...) e53
#    define COT_PP_VARIADIC_ELEM_54(e0, e1, e2, e3, e4, e5, e6, e7, e8, e9, e10, e11, e12, e13, e14, e15, e16, e17, e18, e19, e20, e21, e22, e23, e24, e25, e26, e27, e28, e29, e30, e31, e32, e33, e34, e35, e36, e37, e38, e39, e40, e41, e42, e43, e44, e45, e46, e47, e48, e49, e50, e51, e52, e53, e54, ...) e54
#    define COT_PP_VARIADIC_ELEM_55(e0, e1, e2, e3, e4, e5, e6, e7, e8, e9, e10, e11, e12, e13, e14, e15, e16, e17, e18, e19, e20, e21, e22, e23, e24, e25, e26, e27, e28, e29, e30, e31, e32, e33, e34, e35, e36, e37, e38, e39, e40, e41, e42, e43, e44, e45, e46, e47, e48, e49, e50, e51, e52, e53, e54, e55, ...) e55
#    define COT_PP_VARIADIC_ELEM_56(e0, e1, e2, e3, e4, e5, e6, e7, e8, e9, e10, e11, e12, e13, e14, e15, e16, e17, e18, e19, e20, e21, e22, e23, e24, e25, e26, e27, e28, e29, e30, e31, e32, e33, e34, e35, e36, e37, e38, e39, e40, e41, e42, e43, e44, e45, e46, e47, e48, e49, e50, e51, e52, e53, e54, e55, e56, ...) e56
#    define COT_PP_VARIADIC_ELEM_57(e0, e1, e2, e3, e4, e5, e6, e7, e8, e9, e10, e11, e12, e13, e14, e15, e16, e17, e18, e19, e20, e21, e22, e23, e24, e25, e26, e27, e28, e29, e30, e31, e32, e33, e34, e35, e36, e37, e38, e39, e40, e41, e42, e43, e44, e45, e46, e47, e48, e49, e50, e51, e52, e53, e54, e55, e56, e57, ...) e57
#    define COT_PP_VARIADIC_ELEM_58(e0, e1, e2, e3, e4, e5, e6, e7, e8, e9, e10, e11, e12, e13, e14, e15, e16, e17, e18, e19, e20, e21, e22, e23, e24, e25, e26, e27, e28, e29, e30, e31, e32, e33, e34, e35, e36, e37, e38, e39, e40, e41, e42, e43, e44, e45, e46, e47, e48, e49, e50, e51, e52, e53, e54, e55, e56, e57, e58, ...) e58
#    define COT_PP_VARIADIC_ELEM_59(e0, e1, e2, e3, e4, e5, e6, e7, e8, e9, e10, e11, e12, e13, e14, e15, e16, e17, e18, e19, e20, e21, e22, e23, e24, e25, e26, e27, e28, e29, e30, e31, e32, e33, e34, e35, e36, e37, e38, e39, e40, e41, e42, e43, e44, e45, e46, e47, e48, e49, e50, e51, e52, e53, e54, e55, e56, e57, e58, e59, ...) e59
#    define COT_PP_VARIADIC_ELEM_60(e0, e1, e2, e3, e4, e5, e6, e7, e8, e9, e10, e11, e12, e13, e14, e15, e16, e17, e18, e19, e20, e21, e22, e23, e24, e25, e26, e27, e28, e29, e30, e31, e32, e33, e34, e35, e36, e37, e38, e39, e40, e41, e42, e43, e44, e45, e46, e47, e48, e49, e50, e51, e52, e53, e54, e55, e56, e57, e58, e59, e60, ...) e60
#    define COT_PP_VARIADIC_ELEM_61(e0, e1, e2, e3, e4, e5, e6, e7, e8, e9, e10, e11, e12, e13, e14, e15, e16, e17, e18, e19, e20, e21, e22, e23, e24, e25, e26, e27, e28, e29, e30, e31, e32, e33, e34, e35, e36, e37, e38, e39, e40, e41, e42, e43, e44, e45, e46, e47, e48, e49, e50, e51, e52, e53, e54, e55, e56, e57, e58, e59, e60, e61, ...) e61
#    define COT_PP_VARIADIC_ELEM_62(e0, e1, e2, e3, e4, e5, e6, e7, e8, e9, e10, e11, e12, e13, e14, e15, e16, e17, e18, e19, e20, e21, e22, e23, e24, e25, e26, e27, e28, e29, e30, e31, e32, e33, e34, e35, e36, e37, e38, e39, e40, e41, e42, e43, e44, e45, e46, e47, e48, e49, e50, e51, e52, e53, e54, e55, e56, e57, e58, e59, e60, e61, e62, ...) e62
#    define COT_PP_VARIADIC_ELEM_63(e0, e1, e2, e3, e4, e5, e6, e7, e8, e9, e10, e11, e12, e13, e14, e15, e16, e17, e18, e19, e20, e21, e22, e23, e24, e25, e26, e27, e28, e29, e30, e31, e32, e33, e34, e35, e36, e37, e38, e39, e40, e41, e42, e43, e44, e45, e46, e47, e48, e49, e50, e51, e52, e53, e54, e55, e56, e57, e58, e59, e60, e61, e62, e63, ...) e63


#    define COT_PP_SEQ_ELEM(i, seq) COT_PP_SEQ_ELEM_I(i, seq)
#        define COT_PP_SEQ_ELEM_I(i, seq) COT_PP_SEQ_ELEM_II(COT_PP_SEQ_ELEM_ ## i seq)

#    define COT_PP_SEQ_ELEM_II(im) COT_PP_SEQ_ELEM_III(im)
#    define COT_PP_SEQ_ELEM_III(x, _) x

# define COT_PP_SEQ_ELEM_0(x) x, COT_PP_NIL
# define COT_PP_SEQ_ELEM_1(_) COT_PP_SEQ_ELEM_0
# define COT_PP_SEQ_ELEM_2(_) COT_PP_SEQ_ELEM_1
# define COT_PP_SEQ_ELEM_3(_) COT_PP_SEQ_ELEM_2
# define COT_PP_SEQ_ELEM_4(_) COT_PP_SEQ_ELEM_3
# define COT_PP_SEQ_ELEM_5(_) COT_PP_SEQ_ELEM_4
# define COT_PP_SEQ_ELEM_6(_) COT_PP_SEQ_ELEM_5
# define COT_PP_SEQ_ELEM_7(_) COT_PP_SEQ_ELEM_6
# define COT_PP_SEQ_ELEM_8(_) COT_PP_SEQ_ELEM_7
# define COT_PP_SEQ_ELEM_9(_) COT_PP_SEQ_ELEM_8
# define COT_PP_SEQ_ELEM_10(_) COT_PP_SEQ_ELEM_9
# define COT_PP_SEQ_ELEM_11(_) COT_PP_SEQ_ELEM_10
# define COT_PP_SEQ_ELEM_12(_) COT_PP_SEQ_ELEM_11
# define COT_PP_SEQ_ELEM_13(_) COT_PP_SEQ_ELEM_12
# define COT_PP_SEQ_ELEM_14(_) COT_PP_SEQ_ELEM_13
# define COT_PP_SEQ_ELEM_15(_) COT_PP_SEQ_ELEM_14
# define COT_PP_SEQ_ELEM_16(_) COT_PP_SEQ_ELEM_15
# define COT_PP_SEQ_ELEM_17(_) COT_PP_SEQ_ELEM_16
# define COT_PP_SEQ_ELEM_18(_) COT_PP_SEQ_ELEM_17
# define COT_PP_SEQ_ELEM_19(_) COT_PP_SEQ_ELEM_18
# define COT_PP_SEQ_ELEM_20(_) COT_PP_SEQ_ELEM_19
# define COT_PP_SEQ_ELEM_21(_) COT_PP_SEQ_ELEM_20
# define COT_PP_SEQ_ELEM_22(_) COT_PP_SEQ_ELEM_21
# define COT_PP_SEQ_ELEM_23(_) COT_PP_SEQ_ELEM_22
# define COT_PP_SEQ_ELEM_24(_) COT_PP_SEQ_ELEM_23
# define COT_PP_SEQ_ELEM_25(_) COT_PP_SEQ_ELEM_24
# define COT_PP_SEQ_ELEM_26(_) COT_PP_SEQ_ELEM_25
# define COT_PP_SEQ_ELEM_27(_) COT_PP_SEQ_ELEM_26
# define COT_PP_SEQ_ELEM_28(_) COT_PP_SEQ_ELEM_27
# define COT_PP_SEQ_ELEM_29(_) COT_PP_SEQ_ELEM_28
# define COT_PP_SEQ_ELEM_30(_) COT_PP_SEQ_ELEM_29
# define COT_PP_SEQ_ELEM_31(_) COT_PP_SEQ_ELEM_30
# define COT_PP_SEQ_ELEM_32(_) COT_PP_SEQ_ELEM_31
# define COT_PP_SEQ_ELEM_33(_) COT_PP_SEQ_ELEM_32
# define COT_PP_SEQ_ELEM_34(_) COT_PP_SEQ_ELEM_33
# define COT_PP_SEQ_ELEM_35(_) COT_PP_SEQ_ELEM_34
# define COT_PP_SEQ_ELEM_36(_) COT_PP_SEQ_ELEM_35
# define COT_PP_SEQ_ELEM_37(_) COT_PP_SEQ_ELEM_36
# define COT_PP_SEQ_ELEM_38(_) COT_PP_SEQ_ELEM_37
# define COT_PP_SEQ_ELEM_39(_) COT_PP_SEQ_ELEM_38
# define COT_PP_SEQ_ELEM_40(_) COT_PP_SEQ_ELEM_39
# define COT_PP_SEQ_ELEM_41(_) COT_PP_SEQ_ELEM_40
# define COT_PP_SEQ_ELEM_42(_) COT_PP_SEQ_ELEM_41
# define COT_PP_SEQ_ELEM_43(_) COT_PP_SEQ_ELEM_42
# define COT_PP_SEQ_ELEM_44(_) COT_PP_SEQ_ELEM_43
# define COT_PP_SEQ_ELEM_45(_) COT_PP_SEQ_ELEM_44
# define COT_PP_SEQ_ELEM_46(_) COT_PP_SEQ_ELEM_45
# define COT_PP_SEQ_ELEM_47(_) COT_PP_SEQ_ELEM_46
# define COT_PP_SEQ_ELEM_48(_) COT_PP_SEQ_ELEM_47
# define COT_PP_SEQ_ELEM_49(_) COT_PP_SEQ_ELEM_48
# define COT_PP_SEQ_ELEM_50(_) COT_PP_SEQ_ELEM_49
# define COT_PP_SEQ_ELEM_51(_) COT_PP_SEQ_ELEM_50
# define COT_PP_SEQ_ELEM_52(_) COT_PP_SEQ_ELEM_51
# define COT_PP_SEQ_ELEM_53(_) COT_PP_SEQ_ELEM_52
# define COT_PP_SEQ_ELEM_54(_) COT_PP_SEQ_ELEM_53
# define COT_PP_SEQ_ELEM_55(_) COT_PP_SEQ_ELEM_54
# define COT_PP_SEQ_ELEM_56(_) COT_PP_SEQ_ELEM_55
# define COT_PP_SEQ_ELEM_57(_) COT_PP_SEQ_ELEM_56
# define COT_PP_SEQ_ELEM_58(_) COT_PP_SEQ_ELEM_57
# define COT_PP_SEQ_ELEM_59(_) COT_PP_SEQ_ELEM_58
# define COT_PP_SEQ_ELEM_60(_) COT_PP_SEQ_ELEM_59
# define COT_PP_SEQ_ELEM_61(_) COT_PP_SEQ_ELEM_60
# define COT_PP_SEQ_ELEM_62(_) COT_PP_SEQ_ELEM_61
# define COT_PP_SEQ_ELEM_63(_) COT_PP_SEQ_ELEM_62
# define COT_PP_SEQ_ELEM_64(_) COT_PP_SEQ_ELEM_63
# define COT_PP_SEQ_ELEM_65(_) COT_PP_SEQ_ELEM_64
# define COT_PP_SEQ_ELEM_66(_) COT_PP_SEQ_ELEM_65
# define COT_PP_SEQ_ELEM_67(_) COT_PP_SEQ_ELEM_66
# define COT_PP_SEQ_ELEM_68(_) COT_PP_SEQ_ELEM_67
# define COT_PP_SEQ_ELEM_69(_) COT_PP_SEQ_ELEM_68
# define COT_PP_SEQ_ELEM_70(_) COT_PP_SEQ_ELEM_69
# define COT_PP_SEQ_ELEM_71(_) COT_PP_SEQ_ELEM_70
# define COT_PP_SEQ_ELEM_72(_) COT_PP_SEQ_ELEM_71
# define COT_PP_SEQ_ELEM_73(_) COT_PP_SEQ_ELEM_72
# define COT_PP_SEQ_ELEM_74(_) COT_PP_SEQ_ELEM_73
# define COT_PP_SEQ_ELEM_75(_) COT_PP_SEQ_ELEM_74
# define COT_PP_SEQ_ELEM_76(_) COT_PP_SEQ_ELEM_75
# define COT_PP_SEQ_ELEM_77(_) COT_PP_SEQ_ELEM_76
# define COT_PP_SEQ_ELEM_78(_) COT_PP_SEQ_ELEM_77
# define COT_PP_SEQ_ELEM_79(_) COT_PP_SEQ_ELEM_78
# define COT_PP_SEQ_ELEM_80(_) COT_PP_SEQ_ELEM_79
# define COT_PP_SEQ_ELEM_81(_) COT_PP_SEQ_ELEM_80
# define COT_PP_SEQ_ELEM_82(_) COT_PP_SEQ_ELEM_81
# define COT_PP_SEQ_ELEM_83(_) COT_PP_SEQ_ELEM_82
# define COT_PP_SEQ_ELEM_84(_) COT_PP_SEQ_ELEM_83
# define COT_PP_SEQ_ELEM_85(_) COT_PP_SEQ_ELEM_84
# define COT_PP_SEQ_ELEM_86(_) COT_PP_SEQ_ELEM_85
# define COT_PP_SEQ_ELEM_87(_) COT_PP_SEQ_ELEM_86
# define COT_PP_SEQ_ELEM_88(_) COT_PP_SEQ_ELEM_87
# define COT_PP_SEQ_ELEM_89(_) COT_PP_SEQ_ELEM_88
# define COT_PP_SEQ_ELEM_90(_) COT_PP_SEQ_ELEM_89
# define COT_PP_SEQ_ELEM_91(_) COT_PP_SEQ_ELEM_90
# define COT_PP_SEQ_ELEM_92(_) COT_PP_SEQ_ELEM_91
# define COT_PP_SEQ_ELEM_93(_) COT_PP_SEQ_ELEM_92
# define COT_PP_SEQ_ELEM_94(_) COT_PP_SEQ_ELEM_93
# define COT_PP_SEQ_ELEM_95(_) COT_PP_SEQ_ELEM_94
# define COT_PP_SEQ_ELEM_96(_) COT_PP_SEQ_ELEM_95
# define COT_PP_SEQ_ELEM_97(_) COT_PP_SEQ_ELEM_96
# define COT_PP_SEQ_ELEM_98(_) COT_PP_SEQ_ELEM_97
# define COT_PP_SEQ_ELEM_99(_) COT_PP_SEQ_ELEM_98
# define COT_PP_SEQ_ELEM_100(_) COT_PP_SEQ_ELEM_99
# define COT_PP_SEQ_ELEM_101(_) COT_PP_SEQ_ELEM_100
# define COT_PP_SEQ_ELEM_102(_) COT_PP_SEQ_ELEM_101
# define COT_PP_SEQ_ELEM_103(_) COT_PP_SEQ_ELEM_102
# define COT_PP_SEQ_ELEM_104(_) COT_PP_SEQ_ELEM_103
# define COT_PP_SEQ_ELEM_105(_) COT_PP_SEQ_ELEM_104
# define COT_PP_SEQ_ELEM_106(_) COT_PP_SEQ_ELEM_105
# define COT_PP_SEQ_ELEM_107(_) COT_PP_SEQ_ELEM_106
# define COT_PP_SEQ_ELEM_108(_) COT_PP_SEQ_ELEM_107
# define COT_PP_SEQ_ELEM_109(_) COT_PP_SEQ_ELEM_108
# define COT_PP_SEQ_ELEM_110(_) COT_PP_SEQ_ELEM_109
# define COT_PP_SEQ_ELEM_111(_) COT_PP_SEQ_ELEM_110
# define COT_PP_SEQ_ELEM_112(_) COT_PP_SEQ_ELEM_111
# define COT_PP_SEQ_ELEM_113(_) COT_PP_SEQ_ELEM_112
# define COT_PP_SEQ_ELEM_114(_) COT_PP_SEQ_ELEM_113
# define COT_PP_SEQ_ELEM_115(_) COT_PP_SEQ_ELEM_114
# define COT_PP_SEQ_ELEM_116(_) COT_PP_SEQ_ELEM_115
# define COT_PP_SEQ_ELEM_117(_) COT_PP_SEQ_ELEM_116
# define COT_PP_SEQ_ELEM_118(_) COT_PP_SEQ_ELEM_117
# define COT_PP_SEQ_ELEM_119(_) COT_PP_SEQ_ELEM_118
# define COT_PP_SEQ_ELEM_120(_) COT_PP_SEQ_ELEM_119
# define COT_PP_SEQ_ELEM_121(_) COT_PP_SEQ_ELEM_120
# define COT_PP_SEQ_ELEM_122(_) COT_PP_SEQ_ELEM_121
# define COT_PP_SEQ_ELEM_123(_) COT_PP_SEQ_ELEM_122
# define COT_PP_SEQ_ELEM_124(_) COT_PP_SEQ_ELEM_123
# define COT_PP_SEQ_ELEM_125(_) COT_PP_SEQ_ELEM_124
# define COT_PP_SEQ_ELEM_126(_) COT_PP_SEQ_ELEM_125
# define COT_PP_SEQ_ELEM_127(_) COT_PP_SEQ_ELEM_126
# define COT_PP_SEQ_ELEM_128(_) COT_PP_SEQ_ELEM_127
# define COT_PP_SEQ_ELEM_129(_) COT_PP_SEQ_ELEM_128
# define COT_PP_SEQ_ELEM_130(_) COT_PP_SEQ_ELEM_129
# define COT_PP_SEQ_ELEM_131(_) COT_PP_SEQ_ELEM_130
# define COT_PP_SEQ_ELEM_132(_) COT_PP_SEQ_ELEM_131
# define COT_PP_SEQ_ELEM_133(_) COT_PP_SEQ_ELEM_132
# define COT_PP_SEQ_ELEM_134(_) COT_PP_SEQ_ELEM_133
# define COT_PP_SEQ_ELEM_135(_) COT_PP_SEQ_ELEM_134
# define COT_PP_SEQ_ELEM_136(_) COT_PP_SEQ_ELEM_135
# define COT_PP_SEQ_ELEM_137(_) COT_PP_SEQ_ELEM_136
# define COT_PP_SEQ_ELEM_138(_) COT_PP_SEQ_ELEM_137
# define COT_PP_SEQ_ELEM_139(_) COT_PP_SEQ_ELEM_138
# define COT_PP_SEQ_ELEM_140(_) COT_PP_SEQ_ELEM_139
# define COT_PP_SEQ_ELEM_141(_) COT_PP_SEQ_ELEM_140
# define COT_PP_SEQ_ELEM_142(_) COT_PP_SEQ_ELEM_141
# define COT_PP_SEQ_ELEM_143(_) COT_PP_SEQ_ELEM_142
# define COT_PP_SEQ_ELEM_144(_) COT_PP_SEQ_ELEM_143
# define COT_PP_SEQ_ELEM_145(_) COT_PP_SEQ_ELEM_144
# define COT_PP_SEQ_ELEM_146(_) COT_PP_SEQ_ELEM_145
# define COT_PP_SEQ_ELEM_147(_) COT_PP_SEQ_ELEM_146
# define COT_PP_SEQ_ELEM_148(_) COT_PP_SEQ_ELEM_147
# define COT_PP_SEQ_ELEM_149(_) COT_PP_SEQ_ELEM_148
# define COT_PP_SEQ_ELEM_150(_) COT_PP_SEQ_ELEM_149
# define COT_PP_SEQ_ELEM_151(_) COT_PP_SEQ_ELEM_150
# define COT_PP_SEQ_ELEM_152(_) COT_PP_SEQ_ELEM_151
# define COT_PP_SEQ_ELEM_153(_) COT_PP_SEQ_ELEM_152
# define COT_PP_SEQ_ELEM_154(_) COT_PP_SEQ_ELEM_153
# define COT_PP_SEQ_ELEM_155(_) COT_PP_SEQ_ELEM_154
# define COT_PP_SEQ_ELEM_156(_) COT_PP_SEQ_ELEM_155
# define COT_PP_SEQ_ELEM_157(_) COT_PP_SEQ_ELEM_156
# define COT_PP_SEQ_ELEM_158(_) COT_PP_SEQ_ELEM_157
# define COT_PP_SEQ_ELEM_159(_) COT_PP_SEQ_ELEM_158
# define COT_PP_SEQ_ELEM_160(_) COT_PP_SEQ_ELEM_159
# define COT_PP_SEQ_ELEM_161(_) COT_PP_SEQ_ELEM_160
# define COT_PP_SEQ_ELEM_162(_) COT_PP_SEQ_ELEM_161
# define COT_PP_SEQ_ELEM_163(_) COT_PP_SEQ_ELEM_162
# define COT_PP_SEQ_ELEM_164(_) COT_PP_SEQ_ELEM_163
# define COT_PP_SEQ_ELEM_165(_) COT_PP_SEQ_ELEM_164
# define COT_PP_SEQ_ELEM_166(_) COT_PP_SEQ_ELEM_165
# define COT_PP_SEQ_ELEM_167(_) COT_PP_SEQ_ELEM_166
# define COT_PP_SEQ_ELEM_168(_) COT_PP_SEQ_ELEM_167
# define COT_PP_SEQ_ELEM_169(_) COT_PP_SEQ_ELEM_168
# define COT_PP_SEQ_ELEM_170(_) COT_PP_SEQ_ELEM_169
# define COT_PP_SEQ_ELEM_171(_) COT_PP_SEQ_ELEM_170
# define COT_PP_SEQ_ELEM_172(_) COT_PP_SEQ_ELEM_171
# define COT_PP_SEQ_ELEM_173(_) COT_PP_SEQ_ELEM_172
# define COT_PP_SEQ_ELEM_174(_) COT_PP_SEQ_ELEM_173
# define COT_PP_SEQ_ELEM_175(_) COT_PP_SEQ_ELEM_174
# define COT_PP_SEQ_ELEM_176(_) COT_PP_SEQ_ELEM_175
# define COT_PP_SEQ_ELEM_177(_) COT_PP_SEQ_ELEM_176
# define COT_PP_SEQ_ELEM_178(_) COT_PP_SEQ_ELEM_177
# define COT_PP_SEQ_ELEM_179(_) COT_PP_SEQ_ELEM_178
# define COT_PP_SEQ_ELEM_180(_) COT_PP_SEQ_ELEM_179
# define COT_PP_SEQ_ELEM_181(_) COT_PP_SEQ_ELEM_180
# define COT_PP_SEQ_ELEM_182(_) COT_PP_SEQ_ELEM_181
# define COT_PP_SEQ_ELEM_183(_) COT_PP_SEQ_ELEM_182
# define COT_PP_SEQ_ELEM_184(_) COT_PP_SEQ_ELEM_183
# define COT_PP_SEQ_ELEM_185(_) COT_PP_SEQ_ELEM_184
# define COT_PP_SEQ_ELEM_186(_) COT_PP_SEQ_ELEM_185
# define COT_PP_SEQ_ELEM_187(_) COT_PP_SEQ_ELEM_186
# define COT_PP_SEQ_ELEM_188(_) COT_PP_SEQ_ELEM_187
# define COT_PP_SEQ_ELEM_189(_) COT_PP_SEQ_ELEM_188
# define COT_PP_SEQ_ELEM_190(_) COT_PP_SEQ_ELEM_189
# define COT_PP_SEQ_ELEM_191(_) COT_PP_SEQ_ELEM_190
# define COT_PP_SEQ_ELEM_192(_) COT_PP_SEQ_ELEM_191
# define COT_PP_SEQ_ELEM_193(_) COT_PP_SEQ_ELEM_192
# define COT_PP_SEQ_ELEM_194(_) COT_PP_SEQ_ELEM_193
# define COT_PP_SEQ_ELEM_195(_) COT_PP_SEQ_ELEM_194
# define COT_PP_SEQ_ELEM_196(_) COT_PP_SEQ_ELEM_195
# define COT_PP_SEQ_ELEM_197(_) COT_PP_SEQ_ELEM_196
# define COT_PP_SEQ_ELEM_198(_) COT_PP_SEQ_ELEM_197
# define COT_PP_SEQ_ELEM_199(_) COT_PP_SEQ_ELEM_198
# define COT_PP_SEQ_ELEM_200(_) COT_PP_SEQ_ELEM_199
# define COT_PP_SEQ_ELEM_201(_) COT_PP_SEQ_ELEM_200
# define COT_PP_SEQ_ELEM_202(_) COT_PP_SEQ_ELEM_201
# define COT_PP_SEQ_ELEM_203(_) COT_PP_SEQ_ELEM_202
# define COT_PP_SEQ_ELEM_204(_) COT_PP_SEQ_ELEM_203
# define COT_PP_SEQ_ELEM_205(_) COT_PP_SEQ_ELEM_204
# define COT_PP_SEQ_ELEM_206(_) COT_PP_SEQ_ELEM_205
# define COT_PP_SEQ_ELEM_207(_) COT_PP_SEQ_ELEM_206
# define COT_PP_SEQ_ELEM_208(_) COT_PP_SEQ_ELEM_207
# define COT_PP_SEQ_ELEM_209(_) COT_PP_SEQ_ELEM_208
# define COT_PP_SEQ_ELEM_210(_) COT_PP_SEQ_ELEM_209
# define COT_PP_SEQ_ELEM_211(_) COT_PP_SEQ_ELEM_210
# define COT_PP_SEQ_ELEM_212(_) COT_PP_SEQ_ELEM_211
# define COT_PP_SEQ_ELEM_213(_) COT_PP_SEQ_ELEM_212
# define COT_PP_SEQ_ELEM_214(_) COT_PP_SEQ_ELEM_213
# define COT_PP_SEQ_ELEM_215(_) COT_PP_SEQ_ELEM_214
# define COT_PP_SEQ_ELEM_216(_) COT_PP_SEQ_ELEM_215
# define COT_PP_SEQ_ELEM_217(_) COT_PP_SEQ_ELEM_216
# define COT_PP_SEQ_ELEM_218(_) COT_PP_SEQ_ELEM_217
# define COT_PP_SEQ_ELEM_219(_) COT_PP_SEQ_ELEM_218
# define COT_PP_SEQ_ELEM_220(_) COT_PP_SEQ_ELEM_219
# define COT_PP_SEQ_ELEM_221(_) COT_PP_SEQ_ELEM_220
# define COT_PP_SEQ_ELEM_222(_) COT_PP_SEQ_ELEM_221
# define COT_PP_SEQ_ELEM_223(_) COT_PP_SEQ_ELEM_222
# define COT_PP_SEQ_ELEM_224(_) COT_PP_SEQ_ELEM_223
# define COT_PP_SEQ_ELEM_225(_) COT_PP_SEQ_ELEM_224
# define COT_PP_SEQ_ELEM_226(_) COT_PP_SEQ_ELEM_225
# define COT_PP_SEQ_ELEM_227(_) COT_PP_SEQ_ELEM_226
# define COT_PP_SEQ_ELEM_228(_) COT_PP_SEQ_ELEM_227
# define COT_PP_SEQ_ELEM_229(_) COT_PP_SEQ_ELEM_228
# define COT_PP_SEQ_ELEM_230(_) COT_PP_SEQ_ELEM_229
# define COT_PP_SEQ_ELEM_231(_) COT_PP_SEQ_ELEM_230
# define COT_PP_SEQ_ELEM_232(_) COT_PP_SEQ_ELEM_231
# define COT_PP_SEQ_ELEM_233(_) COT_PP_SEQ_ELEM_232
# define COT_PP_SEQ_ELEM_234(_) COT_PP_SEQ_ELEM_233
# define COT_PP_SEQ_ELEM_235(_) COT_PP_SEQ_ELEM_234
# define COT_PP_SEQ_ELEM_236(_) COT_PP_SEQ_ELEM_235
# define COT_PP_SEQ_ELEM_237(_) COT_PP_SEQ_ELEM_236
# define COT_PP_SEQ_ELEM_238(_) COT_PP_SEQ_ELEM_237
# define COT_PP_SEQ_ELEM_239(_) COT_PP_SEQ_ELEM_238
# define COT_PP_SEQ_ELEM_240(_) COT_PP_SEQ_ELEM_239
# define COT_PP_SEQ_ELEM_241(_) COT_PP_SEQ_ELEM_240
# define COT_PP_SEQ_ELEM_242(_) COT_PP_SEQ_ELEM_241
# define COT_PP_SEQ_ELEM_243(_) COT_PP_SEQ_ELEM_242
# define COT_PP_SEQ_ELEM_244(_) COT_PP_SEQ_ELEM_243
# define COT_PP_SEQ_ELEM_245(_) COT_PP_SEQ_ELEM_244
# define COT_PP_SEQ_ELEM_246(_) COT_PP_SEQ_ELEM_245
# define COT_PP_SEQ_ELEM_247(_) COT_PP_SEQ_ELEM_246
# define COT_PP_SEQ_ELEM_248(_) COT_PP_SEQ_ELEM_247
# define COT_PP_SEQ_ELEM_249(_) COT_PP_SEQ_ELEM_248
# define COT_PP_SEQ_ELEM_250(_) COT_PP_SEQ_ELEM_249
# define COT_PP_SEQ_ELEM_251(_) COT_PP_SEQ_ELEM_250
# define COT_PP_SEQ_ELEM_252(_) COT_PP_SEQ_ELEM_251
# define COT_PP_SEQ_ELEM_253(_) COT_PP_SEQ_ELEM_252
# define COT_PP_SEQ_ELEM_254(_) COT_PP_SEQ_ELEM_253
# define COT_PP_SEQ_ELEM_255(_) COT_PP_SEQ_ELEM_254

# endif