# ifndef COT_PREPROCESSOR_COMPARISON_EQUAL_H
# define COT_PREPROCESSOR_COMPARISON_EQUAL_H
#
# include <preprocessor/not_equal.h>
# include <preprocessor/compl.h>
#
# /* COT_PP_EQUAL */
#

#    define COT_PP_EQUAL(x, y) COT_PP_EQUAL_I(x, y)
#    define COT_PP_EQUAL_I(x, y) COT_PP_COMPL(COT_PP_NOT_EQUAL(x, y))
#
# /* COT_PP_EQUAL_D */
#
# define COT_PP_EQUAL_D(d, x, y) COT_PP_EQUAL(x, y)
#
# endif